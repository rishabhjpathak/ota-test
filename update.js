const fs = require("fs");
const appJson = require("./app.json");

// console.log(appJson);

const version = appJson.expo.version.split(".");

if (version[1] === "9") {
  version[1] = "0";
  version[0] = parseInt(version[0]) + 1;
} else {
  version[1] = parseInt(version[1]) + 1;
}

const versionString = version.join(".");

console.log("New version:", versionString);

appJson.expo.version = versionString;
// console.log(appJson);

fs.writeFileSync("./app.json", JSON.stringify(appJson, null, 2));

process.stdout.emit(versionString);
