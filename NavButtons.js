import React from "react";
import { Alert, Button, View } from "react-native";

export default function NavButtons({ navigation, screen }) {
  const screenText = (screen) => `Go to ${screen} Screen`;

  return (
    <View>
      {screen !== 1 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("First")} onPress={() => navigation.replace("One")} />
        </View>
      )}
      {screen !== 2 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Second")} onPress={() => navigation.replace("Two")} />
        </View>
      )}
      {screen !== 3 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Third")} onPress={() => navigation.replace("Three")} />
        </View>
      )}
      {screen !== 4 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Fourth")} onPress={() => navigation.replace("Four")} />
        </View>
      )}
      {screen !== 5 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Fifth")} onPress={() => navigation.replace("Five")} />
        </View>
      )}
      {/* {screen !== 6 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Sixth")} onPress={() => navigation.replace("Six")} />
        </View>
      )} */}
      {/* {screen !== 7 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Seventh")} onPress={() => navigation.replace("Seven")} />
        </View>
      )} */}
      {/* {screen !== 8 && (
        <View style={{ marginVertical: 10 }}>
          <Button title={screenText("Eight")} onPress={() => navigation.replace("Eight")} />
        </View>
      )} */}
    </View>
  );
}

/*
    navigation.navigate => navigation.replace
*/

// 1 - 3 screens
