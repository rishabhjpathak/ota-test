#!/bin/sh
version=$(node update.js)

echo "Updated App Version to $version"

echo "Publishing to expo..."

expo publish

git add .
git commit -m "Updated App Version to $version"

