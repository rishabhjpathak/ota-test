import React, { useEffect, useState } from "react";
import { ActivityIndicator, Alert, StyleSheet, Text, View } from "react-native";
import NavButtons from "../NavButtons";

import * as Updates from "expo-updates";
import { SafeAreaView } from "react-native-safe-area-context";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import { Dimensions } from "react-native";
import adjust from "../adjust";

export default function Screen({ navigation, text, screen }) {
  const D_Width = Dimensions.get("window").width;
  const D_Height = Dimensions.get("window").height;
  // const [checking, setChecking] = useState(true);
  // const [updateAvailable, setUpdateAvailable] = useState(false);
  // const [downloading, setDownloading] = useState(false);

  // const installUpdate = async () => {
  //   try {
  //     setDownloading(true);
  //     await Updates.fetchUpdateAsync();
  //     setDownloading(false);
  //     Alert.alert(
  //       "App updated successfully",
  //       "Restart on Ok",
  //       [{ text: "OK", onPress: async () => await Updates.reloadAsync() }],
  //       { cancelable: false }
  //     );
  //   } catch (e) {
  //     setDownloading(false);
  //     console.error("Error installing update", e);
  //   }
  // };

  // useEffect(() => {
  //   init();
  // }, []);

  // const init = async () => {
  //   try {
  //     setChecking(true);
  //     console.log("CHECK FOR UPDATE");
  //     const update = await Updates.checkForUpdateAsync();
  //     setChecking(false);
  //     if (update.isAvailable) {
  //       setUpdateAvailable(true);
  //     }
  //   } catch (error) {
  //     setChecking(false);
  //     console.error("Error checking for update", error);
  //   }
  // };

  // return checking ? (
  //   <SafeAreaView
  //     style={{
  //       flex: 1,
  //       flexDirection: "column",
  //       justifyContent: "center",
  //       alignItems: "center",
  //       backgroundColor: "#000",
  //     }}
  //   >
  //     <ActivityIndicator size="large" color="#fff" style={{ paddingRight: 8 }} />
  //     <Text style={{ fontSize: 20, paddingTop: 12, color: "#fff" }}>Checking for updates...</Text>
  //   </SafeAreaView>
  // ) : (
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "space-around", backgroundColor: "#000" }}>
      {/* {updateAvailable && (
        <View style={{ padding: 40 }}>
          <Button
            onPress={() =>
              Alert.alert("Update Available!", "Install the latest version?", [
                {
                  text: "Cancel",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel",
                },
                { text: "OK", onPress: installUpdate },
              ])
            }
            title={downloading ? "Downloading update" : "Update Available"}
            color="#ff0202"
            disabled={downloading}
          />
        </View>
      )} */}
      <View>
        <Text style={{ color: "#0f0", textAlign: "center", fontSize: 20 }}>fontSize: 20</Text>
        <Text style={{ color: "#ff0", textAlign: "center", fontSize: RFValue(20, 640) }}>
          fontSize: RFValue(20, 640)
        </Text>
        <Text style={{ color: "#f00", textAlign: "center", fontSize: RFPercentage(3) }}>fontSize: RFPercentage(3)</Text>
        <Text style={{ color: "#0fffff", textAlign: "center", fontSize: adjust(20) }}>fontSize: adjust(20)</Text>
      </View>
      <View>
        <Text style={{ color: "#fff", textAlign: "center", fontSize: adjust(18) }}>{text}</Text>
        <Text style={{ color: "#0f0", textAlign: "center", fontSize: 20 }}>
          Height: {Number(D_Height.toFixed(2))} | width: {Number(D_Width.toFixed(2))}
        </Text>
        <Text style={{ color: "#ff0", textAlign: "center", fontSize: RFValue(20, 640) }}>
          Height: {Number(D_Height.toFixed(2))} | width: {Number(D_Width.toFixed(2))}
        </Text>
        <Text style={{ color: "#f00", textAlign: "center", fontSize: RFPercentage(3) }}>
          Height: {Number(D_Height.toFixed(2))} | width: {Number(D_Width.toFixed(2))}
        </Text>
        <Text style={{ color: "#0fffff", textAlign: "center", fontSize: adjust(20) }}>
          Height: {Number(D_Height.toFixed(2))} | width: {Number(D_Width.toFixed(2))}
        </Text>
      </View>
      <NavButtons navigation={navigation} screen={screen} />
    </View>
  );
}
