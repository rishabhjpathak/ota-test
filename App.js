import { StatusBar } from "expo-status-bar";
import React, { useEffect, useRef, useState } from "react";
import { ActivityIndicator, Alert, AppState, StyleSheet, Text, View } from "react-native";
import { NavigationContainer, useNavigationContainerRef } from "@react-navigation/native";
import { Overlay } from "react-native-elements";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as Updates from "expo-updates";
import Screen from "./screen/Screen";
import { SafeAreaView } from "react-native-safe-area-context";
import appJson from "./app.json";

const Stack = createNativeStackNavigator();

export default function App() {
  const [checking, setChecking] = useState(false);
  // the below states are global, so that they aren't rerendered every time the screen is changed
  const [downloading, setDownloading] = useState(false);
  const [inFocus, setInfocus] = useState(false);
  const [updateAvailable, setUpdateAvailable] = useState(false);

  useEffect(() => {
    init();
    AppState.addEventListener("change", handleAppStageChange);
  }, []);

  // this code checks for update after app is back in focus
  const handleAppStageChange = (nextAppState) => {
    if (!inFocus && nextAppState === "active") {
      if (!updateAvailable) init();
      setInfocus(true);
    } else {
      setInfocus(false);
    }
  };

  const installUpdate = async () => {
    try {
      setDownloading(true);
      await Updates.fetchUpdateAsync();
      setDownloading(false);
      Alert.alert(
        "App updated successfully",
        "Restart on Ok",
        [{ text: "OK", onPress: async () => await Updates.reloadAsync() }],
        { cancelable: false }
      );
    } catch (e) {
      setDownloading(false);
      console.error("Error installing update", e);
    }
  };

  const init = async () => {
    try {
      setChecking(true);
      console.log("CHECKING FOR UPDATES");
      const update = await Updates.checkForUpdateAsync();
      setChecking(false);
      if (update.isAvailable) {
        setUpdateAvailable(true);
        Alert.alert(
          "Update Available",
          "Press OK to Update the app",
          [{ text: "OK", onPress: () => installUpdate() }],
          {
            cancelable: false,
          }
        );
      } else {
        setUpdateAvailable(false);
      }
    } catch (error) {
      setChecking(false);
      console.error("Error checking for update", error);
    }
  };

  // useEffect(() => {
  //   if (updateAvailable) {
  //     Alert.alert("Update Available", "Press OK to Update the app", [{ text: "OK", onPress: () => installUpdate() }], {
  //       cancelable: false,
  //     });
  //   }
  // }, [updateAvailable]);

  const screenText = (screen) => `Screen ${screen} | Version: ${appJson?.expo?.version}`;
  // const routeNameRef = useRef();
  const navigationRef = useNavigationContainerRef();

  useEffect(() => {
    if (navigationRef) {
      navigationRef.addListener("state", (e) => {
        // const state = e.data.state; //works
        init();
      });
    }
  }, [navigationRef]);

  return (
    // return (
    <>
      {checking ||
        (downloading && (
          <Overlay
            isVisible={checking || downloading}
            collapsable={false}
            fullScreen
            overlayStyle={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#000",
            }}
          >
            <ActivityIndicator size="large" color="#fff" style={{ paddingRight: 8 }} />
            <Text style={{ fontSize: 20, paddingTop: 12, color: "#fff" }}>
              {checking ? "Checking for updates..." : "Downloading Updates..."}
            </Text>
          </Overlay>
        ))}
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          screenOptions={{ headerTitleStyle: { color: "#fff" }, headerStyle: { backgroundColor: "#000" } }}
        >
          {/* <Stack.Screen name="One" component={One} />
        <Stack.Screen name="Two" component={Two} />
        <Stack.Screen name="Three" component={Three} />
        <Stack.Screen name="Four" component={Four} />
        <Stack.Screen name="Five" component={Five} />
        <Stack.Screen name="Six" component={Six} /> */}
          <Stack.Screen
            name="One"
            children={({ navigation }) => <Screen text={screenText("One")} screen={1} navigation={navigation} />}
          />
          <Stack.Screen
            name="Two"
            children={({ navigation }) => <Screen text={screenText("Two")} screen={2} navigation={navigation} />}
          />
          <Stack.Screen
            name="Three"
            children={({ navigation }) => <Screen text={screenText("Three")} screen={3} navigation={navigation} />}
          />
          <Stack.Screen
            name="Four"
            children={({ navigation }) => <Screen text={screenText("Four")} screen={4} navigation={navigation} />}
          />
          <Stack.Screen
            name="Five"
            children={({ navigation }) => <Screen text={screenText("Five")} screen={5} navigation={navigation} />}
          />
          <Stack.Screen
            name="Six"
            children={({ navigation }) => <Screen text={screenText("Six")} screen={6} navigation={navigation} />}
          />
          <Stack.Screen
            name="Seven"
            children={({ navigation }) => <Screen text={screenText("Seven")} screen={7} navigation={navigation} />}
          />
          <Stack.Screen
            name="Eight"
            children={({ navigation }) => <Screen text={screenText("Eight")} screen={8} navigation={navigation} />}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

/*
let unsubscribe;
        unsubscribe = navigation.addListener('focus', () => {
          getHistory();
          setSelectedIndex(0)
        });
        return () => {
          unsubscribe();
        } 
*/
